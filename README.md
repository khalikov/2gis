# Usage (on docker)

docker-compose -f docker-compose.prod.yml up --build

or

docker-compose -f docker-compose.prod.yml up -d --build

# Tests (locally)
python3 -m venv venv

source venv/bin/activate

cd calc_time

pip install -r requirements.txt

export DJANGO_ALLOWED_HOSTS="127.0.0.1 localhost"

export CELERY_BROKER=

export SECRET_KEY=foobar3

python3 manage.py test