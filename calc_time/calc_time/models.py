from django.db import models


class Tuple(models.Model):
    name = models.CharField('Имя сотрудника', max_length=200, null=False,
                            blank=False)
    work_date = models.DateField('Дата')


class AllTime(models.Model):
    tuple = models.ForeignKey(Tuple, models.CASCADE,
                              verbose_name='Сотрудник и дата')
    start_time = models.DateTimeField('Начало работы')
    end_time = models.DateTimeField('Окончание работы')


class CalcTime(models.Model):
    tuple = models.ForeignKey(Tuple, models.CASCADE,
                              verbose_name='Сотрудник и дата')
    sec = models.IntegerField('Рабочее время в секундах')
