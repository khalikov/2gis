import datetime

from django.db.models import Sum

from calc_time.celery import calc
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import View

from calc_time.forms import CalcFileUploadForm
from calc_time.models import CalcTime


def index_view(request):
    return render(request, 'index.html', {})


def handle_uploaded_form(_file):
    username = 'ruslan'
    filepath = (
        f'/home/app/web/calc_dir/{username}-'
        f'{datetime.datetime.now().strftime("%Y%m%d_%H%M%S.%f")}-db.xml'
    )
    with open(filepath, 'wb+') as wfile:
        for chunk in _file.chunks():
            wfile.write(chunk)
    calc.delay(filepath)


class CalcView(View):
    def post(self, request):
        form = CalcFileUploadForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_form(request.FILES['file'])
        else:
            return render(request, 'upload.html',
                          {'errors': form.errors.items()})
        return HttpResponseRedirect('/')

    def get(self, request):
        form = CalcFileUploadForm()
        return render(request, 'upload.html', {'form': form})


class CheckView(View):
    def get(self, request):
        return render(request, 'check.html', {})

    def post(self, request):
        start = request.POST.get('start')
        end = request.POST.get('end')
        print(start, end, 'start and end')
        q = CalcTime.objects.all()
        if start:
            q = q.filter(
                tuple__work_date__gt=datetime.datetime.strptime(
                    start, '%Y-%m-%d')
            )
        if end:
            q = q.filter(
                tuple__work_date__lt=datetime.datetime.strptime(
                    end, '%Y-%m-%d'
                )
            )
        q = q.order_by('tuple__work_date', 'tuple__name')
        group_by_name = request.POST.get('group_by_name')
        group_by = ['tuple__work_date']
        if group_by_name == 'on':
            group_by.append('tuple__name')
        q = q.values(*group_by).annotate(sum=Sum('sec')).order_by(*group_by)
        results = []
        for r0 in q:
            r = {
                'tuple__work_date': r0['tuple__work_date'],
                'hms': str(datetime.timedelta(seconds=r0["sum"]))
            }
            if group_by_name == 'on':
                r['tuple__name'] = r0['tuple__name']
            results.append(r)
        res = {
            'results': results
        }
        if group_by_name == 'on':
            res['checked'] = 'checked'
        if start:
            res['start'] = start
        if end:
            res['end'] = end
        return render(request, 'check.html', res)
