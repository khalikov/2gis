import datetime
import os
import sys
import logging
from lxml import etree
from celery import Celery
from django.conf import settings

from django.db.transaction import atomic

logger = logging.Logger('calc')

# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'calc_time.settings')

app = Celery('tasks', broker=settings.CELERY['BROKER_URL'])

app.conf.update(
    task_serializer='json',
    accept_content=['json'],  # Ignore other content
    result_serializer='json',
    timezone='Europe/Moscow',
    enable_utc=True,
)


def save_time(name, st, en):
    from calc_time.models import Tuple, AllTime, CalcTime

    date_ = st.date()
    # assert date_ == en.date()
    tup, created = Tuple.objects.get_or_create(name=name, work_date=date_)
    times = AllTime.objects.filter(
        tuple__id=tup.id,
        start_time__date=date_,
        end_time__date=date_)
    if times.exists():
        intervals = merge(times, st, en)
        sec = 0
        for interval in intervals:
            sec += interval[1] - interval[0]

        try:
            ct = CalcTime.objects.get(tuple__id=tup.id)
        except CalcTime.DoesNotExist:
            ct = CalcTime(tuple=tup)
        except CalcTime.MultipleObjectsReturned:
            CalcTime.objects.filter(tuple__id=tup.id).delete()
            ct = CalcTime(tuple=tup)
        ct.sec = sec
        ct.save()
    else:
        st.replace(tzinfo=datetime.timezone.utc)
        en.replace(tzinfo=datetime.timezone.utc)
        AllTime(tuple=tup,
                start_time=st,
                end_time=en).save()
        CalcTime(tuple=tup, sec=(en - st).total_seconds()).save()


def merge(times, start_time: datetime.datetime,
          end_time: datetime.datetime):
    # assert start_time.date() == end_time.date()
    arr = []
    for j, atime in enumerate(times):
        dt = datetime.datetime(atime.start_time.year,
                               atime.start_time.month,
                               atime.start_time.day)
        dt = dt.replace(tzinfo=datetime.timezone.utc)
        s1 = (atime.start_time - dt).total_seconds()
        #
        e1 = (atime.end_time - dt).total_seconds()

        arr.append((s1, e1))
        if j == 0:
            s2 = (start_time - dt).total_seconds()
            e2 = (end_time - dt).total_seconds()
            arr.append((s2, e2))
    arr.sort(key=lambda x: x[0])

    # array to hold the merged intervals
    m = []
    big = 10000000
    s = -big
    mx = -big
    for i in range(len(arr)):
        a = arr[i]
        if a[0] > mx:
            if i != 0:
                m.append([s, mx])
            mx = a[1]
            s = a[0]
        else:
            if a[1] >= mx:
                mx = a[1]

    if mx != -big and [s, mx] not in m:
        m.append([s, mx])

    return m


@app.task
@atomic
def calc(file_path, format_='%d-%m-%Y %H:%M:%S'):
    calc_db_path = os.path.expanduser(file_path)
    for event, entry in etree.iterparse(calc_db_path):
        if not entry.tag == 'person':
            continue
        logger.debug(f"start 'person' tag in line {entry.sourceline}")
        name = entry.get('full_name')
        if not entry.get('full_name'):
            logger.log(
                logging.WARNING,
                f"no 'full_name' tag in line {entry.sourceline}"
            )
            continue
        start = entry.find('start')
        end = entry.find('end')
        if start is None or end is None:
            logger.log(
                logging.WARNING,
                f"no 'start' or 'end' tag in person '{name}' "
                f"in line {entry.sourceline}")
            continue
        try:
            start_time = datetime.datetime.strptime(start.text, format_)
            end_time = datetime.datetime.strptime(end.text, format_)
        except ValueError:
            logger.warning(
                f'invalid start or end data in line {entry.sourceline}')
            continue
        if start_time > end_time:
            logger.warning(
                f'start is bigger than end in line {entry.sourceline}')
        start_date = start_time.date()
        end_date = end_time.date()
        dates = [
            start_date + datetime.timedelta(days=x) for x in range(
                0, (end_date - start_date).days + 1)
        ]
        if len(dates) == 1:
            st = start_time
            en = end_time
            st = st.replace(tzinfo=datetime.timezone.utc)
            en = en.replace(tzinfo=datetime.timezone.utc)
            save_time(name, st, en)
        else:
            for j, dt in enumerate(dates):
                if j == 0:
                    st = start_time
                    en = start_date + datetime.timedelta(days=1)
                    en = datetime.datetime(en.year, en.month, en.day)
                elif j == len(dates) - 1:
                    st = datetime.datetime(end_date.year,
                                           end_date.month,
                                           end_date.day)
                    en = end_time
                else:
                    st = datetime.datetime(dt.year, dt.month, dt.day)
                    en = st + datetime.timedelta(days=1)

                st = st.replace(tzinfo=datetime.timezone.utc)
                en = en.replace(tzinfo=datetime.timezone.utc)
                save_time(name, st, en)


def main():
    if len(sys.argv) != 2:
        print('Usage: calc.py db.xml')
        return
    calc(sys.argv[1])


if __name__ == '__main__':
    main()
