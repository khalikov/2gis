from django import forms


class CalcFileUploadForm(forms.Form):
    file = forms.FileField()
