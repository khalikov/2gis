from django.db.models import Max
from django.test import TestCase

from calc_time.celery import calc
from calc_time.models import AllTime, CalcTime, Tuple


class CalcTestCase(TestCase):
    def test_calc(self):
        calc('data/db.xml')

        tuples = Tuple.objects.all().values_list('name', 'work_date')
        assert len(tuples) == 5

        names = Tuple.objects.values('name').annotate(
            mx=Max('work_date')
        ).order_by('name')
        nms = [x['name'] for x in names]
        assert 'i.ivanov' in nms
        assert 'a.stepanova' in nms
        assert 'i.ivanova' in nms
        assert len(nms) == 3

        times = AllTime.objects.all().values_list('tuple__name',
                                                  'tuple__work_date',
                                                  'start_time',
                                                  'end_time')
        assert len(times) == 5

        calcs = CalcTime.objects.all().values_list('tuple__name',
                                                   'tuple__work_date',
                                                   'sec')

        assert len(calcs) == 5
